import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/Components/rounded_field_without_icon.dart';
import 'package:http/http.dart' as http;

import 'package:movie_app/constants.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class DetailPengaduan extends StatefulWidget {
  const DetailPengaduan(
      {Key key,
      this.image,
      this.title,
      this.tahun,
      this.seriesimage,
      this.serirestitle})
      : super(key: key);

  @override
  _DetailPengaduanState createState() => _DetailPengaduanState();

  final image;
  final title;
  final tahun;
  final seriesimage;
  final serirestitle;
}

class _DetailPengaduanState extends State<DetailPengaduan> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                height: size.height * 0.5,
                width: size.width,
                margin: EdgeInsets.only(bottom: 10),
                //color: Colors.red,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                ),
                child: Stack(
                  children: <Widget>[
                    SizedBox(
                      height: size.height * 0.5,
                      width: size.width,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20)),
                        child: Image.network(
                          widget.image,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 30, left: 20),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(bottom: 5),
                child: Container(
                  margin: EdgeInsets.only(
                      left: 20, right: kDefaultPadding - 10, top: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      VerticalDivider(color: Colors.black),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.location_on_outlined),
                            Text(
                              widget.tahun,
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
                indent: 20.0,
                endIndent: 20,
              ),
              Column(
                children: <Widget>[
                  Container(
                    //height: 150,
                    margin: EdgeInsets.only(top: 5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 38, right: 38),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Dee is someone who temporarily " +
                                  "cares for your children on behalf of " +
                                  "you",
                              style: TextStyle(
                                fontFamily: 'Assistant-Light',
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 38, top: 20, bottom: 5),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Language",
                                style: TextStyle(color: kLightGrey),
                              )),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              "English",
                              style: TextStyle(
                                fontFamily: 'Assistant-Light',
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              "Bahasa",
                              style: TextStyle(
                                fontFamily: 'Assistant-Light',
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              "Chinese",
                              style: TextStyle(
                                fontFamily: 'Assistant-Light',
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40, top: 20, bottom: 5),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Race",
                                style: TextStyle(color: kLightGrey),
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40, bottom: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Javanese",
                              style: TextStyle(
                                fontFamily: 'Assistant-Light',
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Text(
                "Rp. 120.000/hr",
                style: TextStyle(
                  fontFamily: 'Assistant-Light',
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontSize: 30,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
