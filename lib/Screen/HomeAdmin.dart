
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:movie_app/Screen/Login.dart';
import 'package:movie_app/Service/ApiService.dart';
import 'package:movie_app/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';



class HomeAdmin extends StatefulWidget {
  final name;
  final id;

  const HomeAdmin({Key key, this.name, this.id}) : super(key: key);

  @override
  _HomeAdminState createState() => _HomeAdminState();
}

class _HomeAdminState extends State<HomeAdmin> {
  bool loginColor = false;
  ApiService _apiService = ApiService();

  Widget _namaakun() {
    return Text("Halo, " + widget.name);
  }

  List _movies = [];
  final String baseurl = "https://imdb8.p.rapidapi.com";
  static const Map<String, String> _headers = {
    "x-rapidapi-key": "3c1b4d6753msh0881b6e74839b9ap101d9djsnaa17135a8c12",
    "x-rapidapi-host": "imdb8.p.rapidapi.com",
  };

  Future<List<Map<String, dynamic>>> getMovie() async {
    final response = await http.get(
        Uri.parse("$baseurl/title/get-most-popular-movies"),
        headers: _headers);
    if (response.statusCode == 200) {
      var data2 =
      json.decode(response.body.toString());
      _movies = data2;

      print(_movies.toString()?.replaceFirst(RegExp(r"\/title/[^]*"), ""));
    } else {
      throw Exception("Error");
    }
  }

  // List film = [];
  // Future<List<Map<String, dynamic>>> getFilm() async {
  //
  //   final response = await http.get(
  //       Uri.parse("$baseurl/title/get-details?tconst=tt0944947"),
  //       headers: _headers);
  //   if (response.statusCode == 200) {
  //     print(response.body);
  //     var data2 =
  //     json.decode(response.body);
  //     print(data2);
  //   } else {
  //     throw Exception("Error");
  //   }
  // }

  Widget _buildMovie(BuildContext context, String images, String name,
      Function onTap) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 250,
          width: 180,
          child: Column(
            children: <Widget>[
              Container(
                height: 190,
                width: 160,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(images),
                  ),
                ),
              ),

              Text(
                name,
                style: TextStyle(fontSize: 17),
              )
            ],
          ),
        ),
      ),
    );
  }

  void initState() {
    super.initState();
    getMovie();
    //getFilm();
  }

  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  void _showSnackBar(String text) {
    _key.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _key,
        //backgroundColor: Colors.blue,

        appBar: AppBar(
          title: Text(
            "MovieNesia",
            style: TextStyle(
                color: kTextColor, fontSize: 30, fontWeight: FontWeight.w300),
          ),
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: kTextColor,
            ),
            onPressed: () {
              _key.currentState.openDrawer();
            },
          ),
        ),
        body: ListView.builder(
          itemCount: _movies.length,
          itemBuilder: (context, int index) {
            return _buildMovie(
                context, _movies[index]??'default', _movies[index]??'default', () {
                  _showSnackBar(_movies[index]);
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) {
              //       return
              //     },
              //   ),
              // );
            });
          },
        ),),
    );
  }
}
