import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/Database/rest-data.dart';
import 'package:movie_app/Model/user.dart';
import 'package:movie_app/Screen/Register.dart';
import '../constants.dart';
import 'HomeAdmin.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

abstract class LoginPageContract {
  void onLoginSuccess(User user);

  void onLoginError(String error);
}

class LoginPagePresenter {
  LoginPageContract _view;
  RestData api = new RestData();

  LoginPagePresenter(this._view);

  doLogin(String username, String password) {
    api
        .login(username, password)
        .then((user) => _view.onLoginSuccess(user))
        .catchError((onError) => _view.onLoginError(onError));
  }
}

class _LoginPageState extends State<LoginPage> implements LoginPageContract {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  String _email, _password;

  bool _isFieldEmailValid;
  bool _isFieldPasswordValid;
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  LoginPagePresenter _presenter;

  _LoginPageState() {
    _presenter = new LoginPagePresenter(this);
  }

  void _register() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return RegisterPage();
        },
      ),
    );
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() {
        _isLoading = true;
        form.save();
        _presenter.doLogin(_email, _password);
      });
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = FlatButton(
      color: Colors.pinkAccent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      minWidth: 200,
      onPressed: () {
        if (_isFieldEmailValid == null ||
            _isFieldPasswordValid == null ||
            !_isFieldEmailValid ||
            !_isFieldPasswordValid) {
          scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("Please fill all field"),
            ),
          );
          return;
        }
        _submit();
      },
      child: Text(
        "Log in",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
      ),
    );
    var registerBtn = FlatButton(
      color: Colors.pink,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      minWidth: 200,
      onPressed: _register,
      child: Text(
        "Register",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
      ),
    );
    var loginForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(20.0),
                child: new TextFormField(
                  controller: _controllerEmail,
                  onSaved: (val) => _email = val,
                  decoration: new InputDecoration(labelText: "Email"),
                  onChanged: (value) {
                    bool isFieldValid = value.trim().isNotEmpty;
                    if (isFieldValid != _isFieldEmailValid) {
                      setState(() {
                        _isFieldEmailValid = isFieldValid;
                      });
                    }
                  },
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(20.0),
                child: new TextFormField(
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password"),
                  onChanged: (value) {
                    bool isFieldValid = value.trim().isNotEmpty;
                    if (isFieldValid != _isFieldPasswordValid) {
                      setState(() {
                        _isFieldPasswordValid = isFieldValid;
                      });
                    }
                  },
                ),
              )
            ],
          ),
        ),
        new Padding(padding: const EdgeInsets.all(10.0), child: loginBtn),
        registerBtn
      ],
    );

    return new Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5),
              child: Text(
                "MovieNesia",
                style: TextStyle(
                    color: CupertinoColors.systemPink,
                    fontSize: 30,
                    fontWeight: FontWeight.w400),
              ),
            ),
            Image.asset(
              'assets/images/rollfilm.JPG',
              height: 25,
              width: 25,
            ),
          ],
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: new Container(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Image.asset("assets/images/rollfilm.JPG"),
                  height: 250,
                  width: 250,
                ),
              ),
              new Center(
                child: loginForm,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onLoginError(String error) {
    // TODO: implement onLoginError
    _showSnackBar("Login not successful");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onLoginSuccess(User user) async {
    // TODO: implement onLoginSuccess
    if (_controllerEmail.text.toString() != user.username && _controllerPassword.text.toString() != user.password) {
      _showSnackBar("Username dan Password Mohon di Cek!");
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) {
            return HomeAdmin(
              id: user.username,
              name: user.name,
            );
          },
        ),
      );
    }
    setState(() {
      _isLoading = false;
    });
    if (user.flaglogged == "logged") {
      print("Logged");
      Navigator.of(context).pushNamed("/home");
    } else {
      print("Not Logged");
    }
  }
}
