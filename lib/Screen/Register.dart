import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/Database/database-helper.dart';
import 'package:movie_app/Model/user.dart';
import 'package:movie_app/Screen/Login.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _name, _username, _password;

  bool _isFieldEmailValid;
  bool _isFieldPasswordValid;
  bool _isFieldNameValid;
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  TextEditingController _controllerName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = FlatButton(
      color: Colors.pinkAccent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(10),
      minWidth: 200,
      onPressed: () {
        if (_isFieldEmailValid == null ||
            _isFieldPasswordValid == null ||
            _isFieldNameValid == null ||
            !_isFieldEmailValid ||
            !_isFieldNameValid ||
            !_isFieldPasswordValid) {
          scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("Please fill all field"),
            ),
          );
          return;
        }
        _submit();
      },
      child: Text(
        "Register",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
      ),
    );

    var loginForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _controllerName,
                  onSaved: (val) => _name = val,
                  decoration: new InputDecoration(labelText: "Name"),
                  onChanged: (value) {
                    bool isFieldValid = value.trim().isNotEmpty;
                    if (isFieldValid != _isFieldNameValid) {
                      setState(() {
                        _isFieldNameValid = isFieldValid;
                      });
                    }
                  },
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _controllerEmail,
                  onSaved: (val) => _username = val,
                  decoration: new InputDecoration(labelText: "Email"),
                  onChanged: (value) {
                    bool isFieldValid = value.trim().isNotEmpty;
                    if (isFieldValid != _isFieldEmailValid) {
                      setState(() {
                        _isFieldEmailValid = isFieldValid;
                      });
                    }
                  },
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _controllerPassword,
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password"),
                  onChanged: (value) {
                    bool isFieldValid = value.trim().isNotEmpty;
                    if (isFieldValid != _isFieldPasswordValid) {
                      setState(() {
                        _isFieldPasswordValid = isFieldValid;
                      });
                    }
                  },
                ),
              )
            ],
          ),
        ),
        Container(margin: EdgeInsets.only(top: 20), child: loginBtn)
      ],
    );

    return new Scaffold(
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: new Container(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(top: 80),
                  child: Image.asset("assets/images/form.JPG"),
                  height: 250,
                  width: 250,
                ),
              ),
              new Center(
                child: loginForm,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() {
        _isLoading = true;
        form.save();
        var user = new User(_name, _username, _password, null);
        var db = new DatabaseHelper();
        db.saveUser(user);
        _isLoading = false;
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return LoginPage();
          }),
        );
      });
    }
  }
}
